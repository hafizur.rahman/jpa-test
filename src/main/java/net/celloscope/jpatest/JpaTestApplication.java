package net.celloscope.jpatest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashSet;

@SpringBootApplication
public class JpaTestApplication implements CommandLineRunner {

	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	@Autowired
	private CustomerRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(JpaTestApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Account account1 = Account.builder()
				.oid("201")
				.accountNo("0200008617777")
				.accountTitle("Shabuz")
				.category("Savings")
				.balance(500.00)
				.build();

		Account account2 = Account.builder()
				.oid("202")
				.accountNo("0200008617778")
				.accountTitle("Hafiz")
				.category("Current")
				.balance(1000.00)
				.build();

		Customer customer = Customer.builder()
				.oid("101")
				.customerId("12365478")
				.name("Hafiz")
				.mobile("01912016260")
				.build();

		customer.setAccounts(new HashSet<Account>(Arrays.asList(account1, account2)));

		repository.save(customer);

		Customer c2 = repository.findById("101").orElseThrow(Exception::new);
		System.out.println(c2.getName());
		System.out.println(c2.getAccounts().size());

	}
}
