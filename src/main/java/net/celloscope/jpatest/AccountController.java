package net.celloscope.jpatest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/v1")
public class AccountController {

    @Autowired
    CustomerRepository custRepo;

    @RequestMapping(value = "/customer/{customerId}", method = GET)
    public ResponseEntity<Customer> generateOtp(@PathVariable("customerId") String customerId) throws Exception {

        Customer customer = custRepo.findById(customerId).orElse(null);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        if (customer != null){
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(customer, httpStatus);
    }

}
