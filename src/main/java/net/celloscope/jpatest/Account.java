package net.celloscope.jpatest;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
@Entity
public class Account {

    @Id
    private String oid;

    private String accountNo;
    private String accountTitle;
    private String category;
    private Double balance;

    @JsonBackReference
    @ManyToMany(mappedBy = "accounts")
    private Set<Customer> customers;
}